/**
 * Created by sanjay on 3/25/15.
 */
App.controller('LoginController', function ($scope, $http, $cookies, $cookieStore, $state,$timeout) {
    //initially set those objects to null to avoid undefined error
    // place the message if something goes wrong
    $scope.account = {};
    $scope.authMsg = '';
    $scope.loginLoader=false;

    $scope.loginAdmin = function () {
        $scope.authMsg = '';
        $scope.loginLoader=true;
        $.post(server_url + '/user_login',
            {
                email: $scope.account.email,
                password: $scope.account.password
            }).then(
            function (data) {
                console.log("hi");
                var response = JSON.parse(data);
                if (response.status != 200) {
                    $scope.loginLoader=false;
                    $scope.authMsg = response.message
                    $timeout(function(){
                        $scope.authMsg = false;
                    },5000)
                    $scope.$apply();
                } else {
                    console.log(">>>>>>>>>>",response.data);
                    var someSessionObj = {
                        'accesstoken': response.data.access_token,
                        'email': response.data.static_email
                    };
                    $cookieStore.put('obj', someSessionObj);
                    $state.go('app.placeorder');
                    $scope.loginLoader=false;
                }
            });
    };

    $scope.logout = function () {
        $cookieStore.remove('obj');
        $state.go('page.login');
    }
});

