/**
 * Created by Navit on 18/02/16.
 */
App.controller('jugnooAdvertisementController', function ($rootScope, $scope, $http, $cookies, $cookieStore, $stateParams, MY_CONSTANT, ngDialog, $timeout, $window,$location, $state, $log) {

    angular.extend($scope, {
        errorMsg: "",
        JA: {}
    });
    var access_toke='';
    if($location.search().access_token){
        access_token = $location.search().access_token;
    }
    $scope.isRegisterd = false;
    $rootScope.globalLoader = false;
    $scope.JA = {};
    $scope.JA.advertising = true;
    $scope.showDetails=0;
    $scope.JA.brand='';


    $scope.validateVehicleNo = function (validVehiceNo) {
        $rootScope.globalLoader = true;
        validVehiceNo == undefined ? false : true;
        if (validVehiceNo) {
            console.log("validation failed");
            return false;
        }
        $http.get('https://test.jugnoo.in:8008/tookan/checkNonJugnooBranding?access_token='+access_token+'&vehicle_no='+$scope.JA.vehicleNo
        ).success(function (data) {
            $rootScope.globalLoader = false;
            console.log(data);
            if (data.flag == 143) {
                $scope.showDetails=data.is_audited;
                $scope.isRegisterd = true;
                $scope.maxID=data.max_id;
                
            }
            else if(data.flag == 144){
                $scope.errorMSG=data.log;
                $scope.isRegisterd = false;
                setTimeout(function () {
                    $scope.$digest()

                });

            }
        }).error();

    }

    var photo = '';
    $scope.allPhotoArr = ['', '', '', '', ''];
    $scope.file_to_upload = function (file, index) {
        $scope.$apply(function () {
            $rootScope.globalLoader = true;
        })
        photo = file[0];
        if(index==0) {
            var formData = new FormData();
            // formData.append("access_token", access_token);
            formData.append("driver_vehicle_no", $scope.JA.vehicleNo);
            // formData.append("ref_image", photo);
            formData.append("doc_type",14);
            formData.append("insert_id",$scope.maxID+1);
            formData.append("pic",photo);
        }
        else {
            var formData = new FormData();
            // formData.append("access_token", access_token);
            formData.append("driver_vehicle_no", $scope.JA.vehicleNo);
            // formData.append("ref_image", photo);
            formData.append("doc_type",13);
            formData.append("insert_id",$scope.maxID+1);
            formData.append("pic",photo);
        }
        $.ajax({
            type: 'POST',
            url: 'https://test.jugnoo.in:8008/tookan/uploadDocs',
            data: formData,
            processData: false,
            dataType: "json",
            contentType: false,
            success: function (response) {
                $rootScope.globalLoader = false;
                $scope.$apply(function () {
                    if (response.flag == 143) {
                        // $scope.allPhotoArr[index] = response.data.ref_image;
                        $rootScope.globalLoader = false;
                    } else {
                        $rootScope.errorMessageGlobal = response.message.toString() || 'Error';
                        $rootScope.globalLoader = false;
                        $timeout(function () {
                            $rootScope.errorMessageGlobal = false;
                        }, 3000)
                    }
                });

            }
        });
    }

    $scope.createJugnooAdvertisement = function () {
        $rootScope.globalLoader = true;
        var createTask = function () {
                $.post('https://test.jugnoo.in:8008/tookan/doNonJugnooBranding',{
                    access_token: access_token,
                    driver_name: $scope.JA.autoDriverName,
                    driver_phone: $scope.JA.driverPhoneNo,
                    vehicle_no: $scope.JA.vehicleNo,
                    city: $scope.JA.city,
                    brand: $scope.JA.brand,
                    no_of_days: $scope.JA.no_of_days,
                    account_no: $scope.JA.account_no,
                    ifsc_code: $scope.JA.ifsc_code,
                    bank_name: $scope.JA.bank_name
                })
                .success(function (data) {
                    $rootScope.globalLoader = false;
                    if (data.flag == 143) {
                        $scope.JA={};
                        $scope.allPhotoArr = newPhotoList = [];
                        // $rootScope.globalLoader = false;
                        $scope.successMSG=true;
                        $scope.successMsg = 'Successful';
                        setTimeout(function () {
                            $scope.$digest();

                        });

                        setTimeout(function () {
                            $state.reload();

                        },5000);

                    } else {
                        $rootScope.errorMessageGlobal = 'Error';
                        $rootScope.globalLoader = false;
                        $scope.successMSG=false;


                        if (data && data.message) {
                            $rootScope.errorMessageGlobal = data.message.toString() || 'Error';
                        }
                        $timeout(function () {
                            $rootScope.errorMessageGlobal = false;
                        }, 3000)
                        setTimeout(function () {
                            $scope.$digest()

                        });
                    }

                })
                .error(function () {
                });

            //}
        }
        createTask();

    }
    $scope.endDateNew = moment().subtract(60,'seconds').format("MM/DD/YYYY HH:mm:ss");
    $scope.startDateNew = moment().format("MM/DD/YYYY");
    $scope.Date = moment($scope.startDateNew).format("MM/DD/YYYY") + " - " +  moment($scope.endDateNew).format("MM/DD/YYYY");

    $scope.maxDate = moment();
    var dateRange=function() {
        $('input[name="daterange"]').daterangepicker({
            "timePicker": true,
            "alwaysShowCalendars": true,
            "timePicker24Hour": true,
            "startDate": $scope.startDateNew,
            "endDate": $scope.endDateNew,
            "maxDate": $scope.maxDate,
            opens: 'right',
        }, function (start, end, label) {
            $scope.dateFlag = 0;
            $scope.startDateNew = start.format('YYYY-MM-DD HH:mm:ss');
            $scope.endDateNew = end.format('YYYY-MM-DD HH:mm:ss');
            $scope.High_Cancellation();
        });

        $('.ranges ul :last-child').hide();
    };
    dateRange();

    $scope.coordsUpdates = 0;
    $scope.dynamicMoveCtr = 0;
    
    

});
