/**
 * Created by Navit on 18/02/16.
 */
App.controller('OrderController', function ($rootScope, $scope, $http, $cookies, $cookieStore, $stateParams,MY_CONSTANT, ngDialog, $timeout, $window, $state, $log) {

    //initial location of the map is set to chandigarh
    $scope.map = {center: {latitude: 30.7500, longitude: 76.7800 }, zoom: 13 };
    $scope.options = {scrollwheel: false};



    angular.extend($scope, {
        pickupLoc:{},
        deliveryLoc:{},
        has_pickup : true,
        account: {}
    });

    jQuery('#pickup_datetimepicker,#delivery_datetimepicker').datetimepicker({
        format: "m/d/Y h:i A",
        minDate: new Date()
    });

    var geocoder = new google.maps.Geocoder();

    $scope.$watch('pickup_add_selector', function(newval, oldval){
        if(newval == "")
        {
            $scope.marker_pickup.options.visible == false;
        }
        else
        {
            $scope.marker_pickup.options.visible == true;
        }

        if(newval == 0){
            $scope.account.pickupAddress = 'Chandigarh';
        }

    });

    $scope.$watch('delivery_add_selector', function(newval, oldval){
        if(newval == "")
        {
            $scope.marker_delivery.options.visible == false;
        }
        else
        {
            $scope.marker_delivery.options.visible == true;
        }
        if(newval == 0){
            $scope.account.deliveryAddress = 'Chandigarh';
        }

    });

    $scope.coordsUpdates = 0;
    $scope.dynamicMoveCtr = 0;

    // Pickup Marker
    $scope.marker_pickup = {
        id: 0,
        coords: {
            latitude: 30.7500,
            longitude: 76.7800
        },
        options: { draggable: true,
        icon: 'app/img/unassigned_pickup.png',
        visible:false},
        events: {
            dragend: function (marker, eventName, args) {
                $log.log('marker dragend');
                var lat = marker.getPosition().lat();
                var lon = marker.getPosition().lng();
                $log.log(lat);
                $log.log(lon);
                $scope.pickupLoc.latitude = lat;
                $scope.pickupLoc.longitude = lon;

                $scope.pickup_add_selector = 1;

                var point = new google.maps.LatLng(lat, lon);

                geocoder.geocode({latLng: point}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $scope.account.pickupAddress = results[0].formatted_address;
                            $scope.$apply();
                        }
                    }
                });

                $scope.marker_pickup.options = {
                    draggable: true,
                    labelAnchor: "100 0",
                    labelClass: "marker-labels",
                    icon: 'app/img/unassigned_pickup.png'
                };
            }
        }
    };



    // Delevery Marker
    $scope.marker_delivery = {
        id: 1,
        coords: {
            latitude: 30.7580,
            longitude: 76.7880
        },
        options: { draggable: true,
            icon: 'app/img/unassigned_delivery.png',
            visible:false},
        events: {
            dragend: function (marker, eventName, args) {
                $log.log('marker dragend');
                var lat = marker.getPosition().lat();
                var lon = marker.getPosition().lng();
                $log.log(lat);
                $log.log(lon);

                $scope.deliveryLoc.latitude = lat;
                $scope.deliveryLoc.longitude = lon;

                $scope.delivery_add_selector = 1;
                var point = new google.maps.LatLng(lat, lon);

                geocoder.geocode({latLng: point}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $scope.account.deliveryAddress = results[0].formatted_address;
                            $scope.$apply();
                        }
                    }
                });

                    $scope.marker_delivery.options = {
                    draggable: true,
                    labelAnchor: "100 0",
                    labelClass: "marker-labels",
                    icon: 'app/img/unassigned_delivery.png'
                };
            }
        }
    };



    $scope.$watchCollection("marker.coords", function (newVal, oldVal) {
        if (_.isEqual(newVal, oldVal))
            return;
        $scope.coordsUpdates++;
    });
    $timeout(function () {
        $scope.marker_pickup.coords = {
            latitude: 30.7500,
            longitude: 76.7800
        };
        $scope.dynamicMoveCtr++;
        $timeout(function () {
            $scope.marker_pickup.coords = {
                latitude: 30.7500,
                longitude: 76.7800
            };
           $scope.dynamicMoveCtr++;
        }, 2000);
    }, 1000);




    $scope.$watch('account.deliveryAddress', function () {
            if ($scope.account.deliveryAddress && $scope.account.deliveryAddress.length) {
                geocoder.geocode({'address': $scope.account.deliveryAddress}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        $timeout(function(){
                            var loc = results[0].geometry.location;
                            $scope.deliveryLoc.latitude = loc.lat() || 0;
                            $scope.deliveryLoc.longitude = loc.lng() || 0;

                            $scope.marker_delivery.coords.latitude = loc.lat() || 0;
                            $scope.marker_delivery.coords.longitude = loc.lng() || 0;

                            $scope.map.center.latitude = loc.lat();
                            $scope.map.center.longitude = loc.lng();


                        });
                    }
                });
            }
    });
    $scope.$watch('account.pickupAddress', function () {
            if ($scope.account.pickupAddress && $scope.account.pickupAddress.length) {
                geocoder.geocode({'address': $scope.account.pickupAddress}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        $timeout(function(){
                            var loc = results[0].geometry.location;
                            $scope.pickupLoc.latitude = loc.lat() || 0;
                            $scope.pickupLoc.longitude = loc.lng() || 0;

                            $scope.marker_pickup.coords.latitude = loc.lat() || 0;
                            $scope.marker_pickup.coords.longitude = loc.lng() || 0;

                            $scope.map.center.latitude = loc.lat();
                            $scope.map.center.longitude = loc.lng();

                        });
                    }
                });
            }
    });


    $scope.placeOrder = function(validOrderId, validorderDetails, validorderInst, validPickupVendorPhone, validDeliveryCustomerPhone, validDeliveryVendorPhone, validPickupVendorName,
    validDeliverUserName, validDeliveryVendorName) {
        console.log("testing");
        // ng-patters return undefined if you never put the data that do not match regex

        validorderDetails == undefined?false:true;
        validOrderId == undefined?false:true;
        validorderInst == undefined?false:true;
        validPickupVendorPhone == undefined?false:true;
        validDeliveryCustomerPhone == undefined?false:true;
        validDeliveryVendorPhone == undefined?false:true;
        validPickupVendorName == undefined?false:true;
        validDeliverUserName == undefined?false:true;
        validDeliveryVendorName == undefined?false:true;

        if($scope.has_pickup==false)
        {
            validPickupVendorPhone = true
        }


            if (validorderDetails || validOrderId || validorderInst ||validPickupVendorPhone ||validDeliveryCustomerPhone || validDeliveryVendorPhone || validPickupVendorName
            ||validDeliverUserName || validDeliveryVendorName) {
                console.log("returnFalse")
                return false;
            }

        if($scope.has_pickup){
            if($scope.account.pickupAddress == ''){
                $rootScope.errorMessageGlobal = 'Pick Up Address is required';
                $timeout(function(){
                    $rootScope.errorMessageGlobal = false;
                },3000)
                return;
            }

            if($scope.account.pickup_date == ''){
                $rootScope.errorMessageGlobal = 'Pick Up Date is required';
                $timeout(function(){
                    $rootScope.errorMessageGlobal = false;
                },3000)
                return;
            }

            if($scope.account.delivery_date == ''){
                $rootScope.errorMessageGlobal = 'Delivery Date is required';
                $timeout(function(){
                    $rootScope.errorMessageGlobal = false;
                },3000)
                return;
            }

            var pickupDate = +new Date($scope.account.pickup_date);
            var deliveryDate = +new Date($scope.account.delivery_date);

            if(deliveryDate<pickupDate){
                $rootScope.errorMessageGlobal = 'Delivery Date should be greater than Pick Up Date';
                $timeout(function(){
                    $rootScope.errorMessageGlobal = false;
                },3000)
                return;
            }



        }

        var meta_data = [
            {
                "label":"Cash to be collected",
                "data_type":"Number",
                "app_side":"0",
                "required":1,
                "value":1,
                "item_id":0,
                "data": $scope.account.cash,
                "template_id":"b2b"},
            {
                "label":"Order ID",
                "data_type":"Text",
                "app_side":"0",
                "required":1,
                "value":1,
                "item_id":0,
                "data":$scope.account.orderId,
                "template_id":"b2b"
            },
            {
                "label":"Vendor Name",
                "data_type":"Text",
                "app_side":"0",
                "required":1,
                "value":1,
                "item_id":0,
                "data":$scope.account.vendorname,
                "template_id":"b2b"
            },
            {
                "label":"Vendor Phone no",
                "data_type":"Telephone",
                "app_side":"0",
                "required":1,
                "value":1,
                "item_id":0,
                "data":$scope.account.vendorphone,
                "template_id":"b2b"
            },{
                "label":"Order Details",
                "data_type":"Text",
                "app_side":"0",
                "required":1,
                "value":1,
                "item_id":0,
                "data":$scope.account.orderdetails,
                "template_id":"b2b"
            }
        ];


        var pickUp_meta_data = [
            {
                "label":"Vendor Name",
                "data_type":"Text",
                "app_side":"0",
                "required":1,
                "value":1,
                "item_id":0,
                "data":$scope.account.pickupVendorname,
                "template_id":"b2b"
            },
            {
                "label":"Vendor Phone no",
                "data_type":"Telephone",
                "app_side":"0",
                "required":1,
                "value":1,
                "item_id":0,
                "data":$scope.account.pickupVendorphone,
                "template_id":"b2b"
            }
        ];


        console.log("in place order")
        $http.post('' + 'https://api2.tookanapp.com:5555/create_task', {
                "access_token": '9d7f3d0aefbd33cce49e6d55c6ab4a67',
                "customer_email": $scope.account.customer_email,
                "customer_username": $scope.account.recepname,
                "customer_phone": $scope.account.recepephone,
                "customer_address": $scope.account.deliveryAddress,
                "job_description": $scope.account.orderdetails,
                "job_delivery_datetime": $scope.account.delivery_date,
                "fleet_id": '',
                "latitude": $scope.deliveryLoc.latitude,
                "longitude": $scope.deliveryLoc.longitude,
                "timezone": '-330',
                "job_pickup_latitude": $scope.pickupLoc.latitude,
                "job_pickup_longitude": $scope.pickupLoc.longitude,
                "job_pickup_address": $scope.account.pickupAddress ,
                "job_pickup_phone": '',
                "job_pickup_name": '',
                "job_pickup_datetime": $scope.account.pickup_date,
                "job_pickup_email": '',
                "has_pickup": $scope.has_pickup ? 1: 0,
                "has_delivery": 1,
                meta_data: meta_data,
                pickup_meta_data: pickUp_meta_data,
                "layout_type": 0,
                "auto_assignment": 0,
                "team_id": ''
            }, {timeout: 30000}
        ).success(function (data) {
            if (data.status == 200) {
                $scope.account = {};
                $rootScope.globalLoader = false;
                $rootScope.successMessageGlobal = 'Task created successfully';
                $scope.pickup_add_selector = '';
                $scope.delivery_add_selector = '';

                $timeout(function(){
                    $rootScope.successMessageGlobal = false;
                },3000)

            } else {
                $rootScope.globalLoader = false;
                $rootScope.errorMessageGlobal = data.message.toString();
                $timeout(function(){
                    $rootScope.errorMessageGlobal = false;
                },3000)
            }
        }).error();
    }


$scope.hasPickupEnabled = function(value)
{
    if(value==true)
    {
        $('.angular-google-map-container').css('height', '780px');
    }
    else
    {
        $('.angular-google-map-container').css('height', '500px');
    }
}

$scope.todayDate = new Date();
    // regex for alpha numeric with @ - / . #
    $scope.regex1= '/^[ A-Za-z0-9@-/.#]*$/'
    console.log( $scope.regex1)

});


//function checkAddress(first_box,second_box)
//{
//    if (first_box.checked && second_box.checked)
//    {
//        alert("both");
//    }
//    else if(first_box.checked && !second_box.checked){
//        alert("first");
//    }
//    else if(!first_box.checked && second_box.checked){
//        alert("second");
//    }
//    else{
//        alert("none");
//    }
//}

//$scope.checkAddress = function()
//{
//    var first_box = document.getElementById('pickup_box');
//    var second_box = document.getElementById('delivery_box');
//    var pickup_form = document.getElementById('pickup_box');
//    var delivery_form = document.getElementById('delivery_box');
//    if (first_box.checked && second_box.checked)
//    {
//        $scope.pickupClass = "ng-show";
//        $scope.deliveryClass = "ng-show";;
//    }
//    else if(first_box.checked && !second_box.checked){
//        $scope.pickupClass = "ng-show";
//        $scope.deliveryClass ="ng-hide";
//    }
//    else if(!first_box.checked && second_box.checked){
//        $scope.pickupClass ="ng-hide";
//        $scope.deliveryClass = "ng-show";
//    }
//    else{
//        $scope.pickupClass ="ng-hide";
//        $scope.deliveryClass ="ng-hide";
//    }
//}
