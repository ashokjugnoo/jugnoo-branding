// To run this code, edit file
// index.html or index.jade and change
// html data-ng-app attribute from
// angle to myAppName
// -----------------------------------

var myApp = angular.module('myAppName', ['angle','uiGmapgoogle-maps']);

myApp.config(['uiGmapGoogleMapApiProvider', function (GoogleMapApi) {
    GoogleMapApi.configure({
//    key: 'your api key',
        v: '3.17',
        libraries: 'weather,geometry,visualization'
    });
}]);
myApp.run(["$log", function ($log) {

    $log.log('I\'m a line from custom.js');

}]);

App.constant("MY_CONSTANT", {
    "url": ""
});

App.constant("responseCode", {
    "SUCCESS": 200
});
myApp.config(['$stateProvider', '$locationProvider', '$urlRouterProvider', 'RouteHelpersProvider','$httpProvider',
    function ($stateProvider, $locationProvider, $urlRouterProvider, helper, $httpProvider) {
        'use strict';

        // Set the following to true to enable the HTML5 Mode
        // You may have to set <base> tag in index and a routing configuration in your server
        $locationProvider.html5Mode(false);

        // default route
        $urlRouterProvider.otherwise('/app/jugnooAdvertisement');
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/json';
        $httpProvider.defaults.timeout = 10000;

        //
        // Application Routes
        // -----------------------------------
        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: helper.basepath('app.html'),
                controller: 'AppController',
                resolve: helper.resolveFor('modernizr', 'icons', 'screenfull')
            })
            .state('app.placeorder', {
                url: '/placeorder',
                title: 'Orders',
                templateUrl: helper.basepath('placeorder.html'),
                resolve: helper.resolveFor('parsley','inputmask','datatables', 'datatables-pugins','ngDialog')
            })

            .state('app.jugnooAdvertisement', {
                url: '/jugnooAdvertisement?access_token',
                title: 'Jugnoo Advertisement',
                templateUrl: helper.basepath('jugnooAdvertisement.html'),
                resolve: helper.resolveFor('parsley','inputmask','datatables', 'datatables-pugins','ngDialog')
            })

    }]);
